//
//  kadai11ViewModel.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/29.
//

import UIKit

//Podからインスールしたライブラリを使うのでインポートします。
import Alamofire

class kadai11ViewModel: ObservableObject {

    @Published var weather: Weather?
/*
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var seachButton: UIButton!
    @IBOutlet weak var resultLabel: UILabel!

    @IBAction func tapSeachButton(_ sender: Any) {
        self.getAddress(zipCode: zipCodeTextField.text)
    }
*/

    func requestAPI() {
        //お天気APIから東京の天気を取得する
        let url: String = "https://www.jma.go.jp/bosai/forecast/data/overview_forecast/130000.json"
        AF.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            guard let data = response.data else { return }
            do {
                try self.parse(data: data)
            } catch let error {
                print("Error: \(error)")
                self.printError()
            }
        }
    }

    func parse(data:Data) throws {
        self.weather = try JSONDecoder().decode(Weather.self, from: data)
        print("\(weather?.targetArea ?? "")")
    }

    func printError(){
        print("検索結果が取得できませんでした")
    }
}

