//
//  kadai11Model.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/29.
//

import UIKit

class Weather: Codable {
    let targetArea: String
    let headlineText: String
    let text: String
}
/*
class Weather: Codable {
    let title: String
    let link: String
    let description: Description
    let pinpointLocations: [PinpointLocation]
    let location: Location
    let forecasts: [Forcast]
}

struct Description: Codable {
    let text: String
    let publicTime: String
}

struct PinpointLocation: Codable {
    let link: String
    let name: String
}

struct Location: Codable {
    let city: String
    let area: String
    let prefecture: String
}

struct Forcast: Codable {
    let dateLabel: String
    let telop: String
    let date: String
    let image: Mark
}

struct Mark: Codable {
    let width: Int
    let url: String
    let title: String
    let height: Int
}
*/

