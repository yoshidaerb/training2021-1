//
//  kadai11View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/29.
//

import SwiftUI
import UIKit

struct kadai11View: View {
    // 観測するオブジェクト
    @ObservedObject var viewModel = kadai11ViewModel()
    
    var body: some View {
        VStack{
            Text("東京の天気").padding()
            Text(viewModel.weather?.headlineText ?? "").font(.callout)
            Text(viewModel.weather?.text ?? "").font(.caption2)
            // 実行ボタン
            Button{
                viewModel.requestAPI()
            }label:{
                Text("実行")
            }
        }
    }
}

