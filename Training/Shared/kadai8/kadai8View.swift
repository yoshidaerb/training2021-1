//
//  kadai8View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import SwiftUI
import UIKit

struct kadai8View: View {
    // 観測するオブジェクト
    @ObservedObject var viewModel = kadai8ViewModel()
    @State var selectedRatio = 0
    
    var body: some View {
        VStack{
            // セグメントコントロール
            Picker(selection: $selectedRatio, label: Text("Color")){
                Text("通常").tag(0)
                Text("軽減税率").tag(1)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding()
            // 税率表示
            Text("\(viewModel.ratio)%")
                .frame(width: 50, height: 50)
                .padding()
            // 実行ボタン
            Button(action: {
                // updateratioの呼び出し
                viewModel.updateRatio(num:selectedRatio)
            }) {
                Text("実行")
            }
            .frame(width:200)
        }
    }
}

