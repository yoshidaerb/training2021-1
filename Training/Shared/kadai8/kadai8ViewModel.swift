//
//  kadai8ViewModel.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//
import Foundation

class kadai8ViewModel: ObservableObject {
    // 税率の変数をPublishで定義
    @Published var ratio = 10
    // 税率更新関数
    func updateRatio(num:Int){
        switch num {
        case 0 :
            ratio = 10
        case 1 :
            ratio = 8
        default:
            ratio = 10
        }
    }
}
