//
//  ContentView3.swift
//  Training (iOS)
//
//  Created by 野呂彩香 on 2021/07/07.
//

import SwiftUI
import UIKit

struct ContentView3: View {
    var body: some View {
        VStack{
            Image("古橋　省吾_2021-07-02_14-00-26_298")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .scaleEffect(1.2)
                .offset(x:0,y:-10)
                .frame(width:200,height:200)
                .clipShape(RoundedRectangle(cornerRadius: 20))
                .shadow(radius: 20)
                .overlay(
                    Text("ccs")
                        .foregroundColor(.white)
                )
            HStack{
                Ellipse()
                    .foregroundColor(.orange)
                    .frame(width:100,height:150)
                    .rotationEffect(.degrees(60))
                    .clipped()
                    .rotationEffect(.degrees(-45),anchor: .bottom)
                ZStack{
                    Ellipse()
                        .stroke(lineWidth: 4)
                        .foregroundColor(.pink)
                        .frame(width:40,height:120)
                    Ellipse()
                        .stroke(lineWidth: 4)
                        .foregroundColor(.purple)
                        .frame(width:40,height:120)
                        .rotationEffect(.degrees(30),anchor: .bottom)
                    Ellipse()
                        .stroke(lineWidth: 4)
                        .foregroundColor(.green)
                        .frame(width:40,height:120)
                        .rotationEffect(.degrees(-30),anchor: .bottom)
                }
            }
            HStack{
                Flower()
                    .frame(height:50)
                Text("ntt \n data \n ccs")
                    .multilineTextAlignment(.center)
                    .rotation3DEffect(.degrees(45), axis: (x:1,y:0,z:0))
                Flower()
                    .frame(height:50)
            }
            Image(systemName:"pencil")
        }
    }
}

struct Flower: View {
    var body: some View{
        Text("花")
            .foregroundColor(.pink)
            .fontWeight(.thin)
    }
}
