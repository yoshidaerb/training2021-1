//
//  kadai7View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import SwiftUI

struct kadai7View: View {
    // テキストフィールドの値を格納する場所
    @State var code = ""
    @State var name = ""
    
    var body: some View {
        NavigationView {
            VStack{
                HStack{
                    Text("商品コード:")
                    // 商品コード入力テキストフィールド
                    TextField("",text:$code)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width:150)
                }
                HStack{
                    Text("商品名:")
                    // 商品名入力テキストフィールド
                    TextField("",text:$name)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width:150)
                }
                // Sub7Viewのリンク
                NavigationLink(destination: Sub7View(code:code,name:name)){
                    Text("実行").frame(width:200)
                }
                .padding()
            }
        }
    }
}



