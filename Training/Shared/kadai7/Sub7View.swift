//
//  Sub7View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import SwiftUI

struct Sub7View: View {
    // kadai7Viewの変数とバインディングする変数
    @State var code:String
    @State var name:String
    
    var body: some View {
        VStack{
            // 商品コード随時更新表示
            Text(code).frame(width:200).padding()
            // 商品名随時更新表示
            Text(name).frame(width:200)
        }
    }
}
