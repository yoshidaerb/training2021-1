//
//  TrainingApp.swift
//  Shared
//
//  Created by 野呂彩香 on 2021/07/02.
//

import SwiftUI

@main
struct TrainingApp: App {
    var body: some Scene {
        WindowGroup {
            kadai10View()
        }
    }
}

