//
//  kadai10View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/26.
//

import SwiftUI

struct kadai10View: View {
    // 観測するオブジェクト
    @ObservedObject var viewModel = kadai10ViewModel()
    
    var body: some View {
        NavigationView {
            VStack{
                HStack{
                    // 商品名入力テキストフィールド
                    TextField("商品名",text:$viewModel.selectName)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width:100)
                        .padding()
                    // 検索ボタン
                    Button(action:{
                        // 商品名で商品を検索
                        viewModel.makeList(viewModel.selectName)
                    }) {
                        Text("検索")
                            .frame(width: 100, height: 40, alignment: .center)
                            .foregroundColor(Color.white)
                            .background(Color.blue)
                            .cornerRadius(10,antialiased: true)
                    }
                }
                // 検索結果(商品リスト)
                List {
                    ForEach(viewModel.productList, id:\.self) { product in
                        HStack{
                            // 商品コードを表示
                            Text(product.code).frame(width:80)
                            // 商品名を表示
                            Text(product.name).frame(width:80)
                            // 遷移ボタン
                            NavigationLink(destination: kadai9View(viewModel: kadai9ViewModel(product:product))){
                                Text("遷移")
                                    .frame(width: 80, height: 40, alignment: .center)
                                    .foregroundColor(Color.white)
                                    .background(Color.blue)
                                    .cornerRadius(10,antialiased: true)
                            }
                        }
                    }
                }
            }
        }.onAppear{
            viewModel.makeList(viewModel.selectName)
        }
    }
}
