//
//  kadai10Model.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import Foundation
import RealmSwift

class kadai10Model {
    // Realmを起動
    private var realm1 = try! Realm()
    // 商品名で商品を検索
    func findByName(_ selectName:String) -> Array<Product> {
        var productList:Array<Product> = []
        //do {
            // 戻り値の型を準備
            let results:Results<Product>
            // 検索方法を分岐
            if selectName == "" {
                // 商品名あり
                results = realm1.objects(Product.self)
            }else{
                // 商品名なし
                results = realm1.objects(Product.self).filter("name CONTAINS %@",selectName)
            }
            // 検索結果の件数を取得します。
            let count = results.count
            if (count == 0) {
                // 検索結果0件の場合
            } else {
                // 検索結果がある場合productListにデータを代入
                for result in results {
                    productList.append(result)
                }
            }
            
        //}catch{
            //print("Error \(error)")
        //}
        return productList
    }
}

