//
//  kadai10ViewModel.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import Foundation
import RealmSwift

class kadai10ViewModel: ObservableObject {
    // 変数をPublishで定義
    @Published var productList:[Product] = []
    @Published var selectName:String = ""
    // リスト作成
    func makeList(_ selectName:String){
        productList = kadai10Model().findByName(selectName)
    }
}
