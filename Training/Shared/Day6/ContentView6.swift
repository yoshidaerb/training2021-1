//
//  SwiftUIView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/13.
//

import SwiftUI
import UIKit

struct ContentView6: View {
    //@ObservedObject var viewModel6 = ContentViewModel6()
    @State var isShow1 = true
    @State var isShow2 = true
    @State var opaValue = 1.0
    @State var btnText = "消す"
    @State var selectedSize = 2
    @State var selectedColor1 = 0
    @State var selectedColor2 = 0
    @State var sizes = ["XS","S","M","L","LL"]
    @State var colors = ["Red","Green","Blue","Yellow","Pink","White"]
    let colorViews = [Color.red,Color.green,Color.blue]
    var body: some View {
        VStack(alignment: .center){
            HStack{
                Toggle(isOn:$isShow1){
                    Text("表示/非表示")
                }
                .fixedSize()
                Text("　 ")
                Button(action:{
                    isShow2.toggle()
                    btnText = isShow2 ? "消す" : "表示する"
                    withAnimation(.easeInOut(duration: 2)){
                        opaValue = isShow2 ? 1.0 : 0.0
                    }
                })
                {
                    Text(btnText)
                }
                Text("　 ")
            }
            HStack{
                if isShow1 {
                    Image("古橋　省吾_2021-07-02_14-00-26_298")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width:80,height: 80)
                } else{
                    Text("")
                        .frame(width:80,height: 80)
                }
                Text("　　　")
                Image("古橋　省吾_2021-07-02_14-00-26_298")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width:80,height: 80)
                    .opacity(opaValue)
            }
        }
        HStack{
            // Sizeのピッカー
            Picker(selection: $selectedSize, label: Text("Size")){
                ForEach(0..<sizes.count){index in
                    Text(sizes[index])
                }
            }
            .frame(width:150)
            .padding(.horizontal,10)
            .clipped()
            // Colorのピッカー
            Picker(selection: $selectedColor1, label: Text("Color")){
                ForEach(0..<colors.count){index in
                    Text(colors[index])
                }
            }
            .frame(width:150)
            .padding(.horizontal,10)
            .clipped()
            .onAppear(perform: {
                selectedColor1 = colors.count/2
            })
        }
        HStack{
            Text("size:\(sizes[selectedSize])")
                .padding(.horizontal,40)
            Text("color:\(colors[selectedColor1])")
                .padding(.horizontal,30)
        }
        .foregroundColor(Color.white)
        .background(
            RoundedRectangle(cornerRadius: 20)
                .frame(width:300,height: 30)
                .foregroundColor(Color.black)
        )
        VStack{
            Picker(selection: $selectedColor2, label: Text("Color")){
                Text("Red").tag(0)
                Text("Green").tag(1)
                Text("Blue").tag(2)
            }
            .pickerStyle(SegmentedPickerStyle())
            symbolImage(num:selectedColor2)
                .resizable()
                .foregroundColor(colorViews[selectedColor2])
                .frame(width: 50, height: 50)
                .padding()
        }
        .padding()
    }
    
    func symbolImage(num:Int) -> Image{
        switch num {
        case 0 :
            return Image(systemName: "r.circle")
        case 1 :
            return Image(systemName: "g.circle")
        case 2 :
            return Image(systemName: "b.circle")
        default:
            return Image(systemName: "r.circle")
        }
    }
}

