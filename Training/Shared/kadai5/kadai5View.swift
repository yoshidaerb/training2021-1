//
//  kadai5View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import SwiftUI

struct kadai5View: View {
    // シートが開いている状態
    @State var isShow: Bool = false
    
    var body: some View {
        // 実行ボタン
        Button(action: {
            isShow = true
        }) {
            Text("実行")
        }
        .padding()
        .sheet(isPresented: $isShow){
            // シートを作る
            CloseView()
        }
    }
}

struct CloseView: View {
    // 共有オブジェクトを指定する
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
            VStack{
                Text("画面を閉じます").padding()
                // 閉じるボタン
                Button(action:{
                    // 閉じる時は@Environmentで制御
                    self.presentationMode.wrappedValue.dismiss()
                },label:{
                    Text("閉じる")
                })
            }
    }
}


