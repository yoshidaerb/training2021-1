//
//  ContentDetailView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/09.
//

import SwiftUI

struct ContentDetailView: View {
    var day:DayData
    var body: some View {
        VStack{
            Image(day.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
            Text(day.day)
            Spacer()
        }
        .padding()
        .navigationTitle(Text(verbatim: day.day))
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct ContentDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ContentDetailView(day:weekArray[0])
    }
}
