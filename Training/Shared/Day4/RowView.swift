//
//  RowView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/09.
//

import SwiftUI

struct RowView: View {
    var day:DayData
    var body: some View {
        HStack{
            Image(day.imageName)
                .resizable()
                .frame(width:80,height: 80)
            Text(day.day)
            Spacer()
        }
    }
}

struct RowView_Previews: PreviewProvider {
    static var previews: some View {
        RowView(day:weekArray[0])
            .previewLayout(.fixed(width: 300, height: 80))
    }
}
