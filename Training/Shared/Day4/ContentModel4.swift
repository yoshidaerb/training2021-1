//
//  ContentModel4.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/09.
//

import Foundation

// 写真データを配列に代入
var weekArray:[DayData] = makeData()

// 写真データを構造体で定義
struct DayData: Identifiable{
    var id: Int
    var imageName:String
    var day:String
}

//
func makeData()->[DayData]{
    var dataArray:[DayData] = []
    dataArray.append(DayData(id:1, imageName:"MicrosoftTeams-image", day:"月"))
    dataArray.append(DayData(id:2, imageName:"MicrosoftTeams-image (1)", day:"火"))
    dataArray.append(DayData(id:3, imageName:"MicrosoftTeams-image (2)", day:"水"))
    dataArray.append(DayData(id:4, imageName:"MicrosoftTeams-image (3)", day:"木"))
    dataArray.append(DayData(id:5, imageName:"MicrosoftTeams-image (4)", day:"金"))
    dataArray.append(DayData(id:6, imageName:"MicrosoftTeams-image (5)", day:"土"))
    dataArray.append(DayData(id:7, imageName:"MicrosoftTeams-image (6)", day:"日"))
    return dataArray
}
