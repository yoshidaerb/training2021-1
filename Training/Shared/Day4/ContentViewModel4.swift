//
//  ContentViewModel4.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/09.
//

import Foundation

class ContentViewModel4: ObservableObject {
    @Published var weekdays = ["月","火","水","木","金"]
    @Published var weekend = ["土","日"]
}
