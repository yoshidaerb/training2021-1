//
//  ContentView4.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/09.
//

import SwiftUI
import UIKit

struct ContentView4: View {
    @ObservedObject var viewModel4 = ContentViewModel4()
    var body: some View {
        NavigationView {
            List(weekArray) { item in
                NavigationLink(destination: ContentDetailView(day: item)){
                RowView(day: item)
                }
            }
            .navigationTitle(Text("来週"))
            /*
            List {
                Section(header: Text("平日").font(.largeTitle).padding(.top),
                        footer: Text("7/12-7/16")) {
                    ForEach(0 ..< viewModel4.weekdays.count) { index in
                        Text(self.viewModel4.weekdays[index])
                    }
                }
                Section(header: Text("休日").font(.largeTitle).padding(.top),
                        footer: Text("7/17-7/18")) {
                    ForEach(0 ..< viewModel4.weekend.count) { index in
                        Text(self.viewModel4.weekend[index])
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .navigationTitle("来週")
            .navigationBarTitleDisplayMode(.inline)
            */
        }
    }
}
