//
//  ContentView.swift
//  Shared
//
//  Created by 野呂彩香 on 2021/07/02.
//

import SwiftUI
import UIKit

struct ContentView: View {
    @ObservedObject var viewModel = ContentViewModel()
    var body: some View {
        VStack {
            Text(viewModel.message1)
                .padding()
            Text(viewModel.message)
                .padding()
            HStack{
                Text("\(viewModel.num1)")
                Text("\(viewModel.num2)")
                Text("\(viewModel.num3)")
                Text("\(viewModel.num4)")
            }
            HStack{
                Button(action: {
                    // viewModelのアクションを呼び出す
                    viewModel.updateNum1()
                }){
                    Text("月")
                        .font(.largeTitle)
                }
                
                Button(action: {
                    // viewModelのアクションを呼び出す
                    viewModel.updateNum2()
                }){
                    Text("月")
                        .font(.largeTitle)
                }
                
                Button(action: {
                    // viewModelのアクションを呼び出す
                    viewModel.updateNum3()
                }){
                    Text("日")
                        .font(.largeTitle)
                }
                
                Button(action: {
                    // viewModelのアクションを呼び出す
                    viewModel.updateNum4()
                }){
                    Text("日")
                        .font(.largeTitle)
                }
            }
            Button(action: {
                // viewModelのアクションを呼び出す
                viewModel.reset()
            }){
                Text("リセット")
                    .font(.largeTitle)
            }
            Button(action: {
                // viewModelのアクションを呼び出す
                viewModel.checkNumUpdateMessage()
                viewModel.printcalendar()
            }){
                Text("決定")
                    .font(.largeTitle)
            }
            /*
             List(self.viewModel.persons) { person in
                             Button(action: {
                                 self.viewModel.showDetail(person: person)
                             }, label: {
                                 HStack {
                                     Text(person.id.description)
                                     Text(person.name)
                                     Spacer()
                                 }
                             })
                             .foregroundColor(.primary)
                             .alert(isPresented: self.$viewModel.isShowDetail) {
                                 Alert(title: Text("人物詳細"), message: Text(self.viewModel.message), dismissButton: .default(Text("OK")))
                             }
                         }
                     }
                     .onAppear() {
                         self.viewModel.loadPersons()
                     }
             */
        }
    }
}

