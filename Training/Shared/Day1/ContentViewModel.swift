//
//  ContentViewModel.swift
//  Training
//
//  Created by 野呂彩香 on 2021/07/05.
//

import Foundation

class ContentViewModel: ObservableObject {
    @Published var message1:String = "僕の誕生日は？"
    @Published var message:String = ""
    @Published var num1:Int = 0
    @Published var num2:Int = 0
    @Published var num3:Int = 0
    @Published var num4:Int = 0
    
    func updateNum1(){
        num1 += 1
    }
    
    func updateNum2(){
        num2 += 1
    }
    
    func updateNum3(){
        num3 += 1
    }
    
    func updateNum4(){
        num4 += 1
    }
    
    func reset(){
        num1 = 0
        num2 = 0
        num3 = 0
        num4 = 0
    }
    
    func checkNumUpdateMessage(){
        if(num1 == 1 && num2 == 0 && num3 == 1 && num4 == 3){
            message = "大正解です！"
        }else{
            message = "違いますー"
        }
    }
    
    func printcalendar(){
        let start = 3
        let end = 31
        var numList = [0,0,0,0,0,0,0]
        for j in 0 ... 5 {
            for i in 0 ... 6 {
                numList[i] = i + j * 7 - start + 1
                if(numList[i] <= 0 || numList[i] > end){
                    numList[i] = 0
                }
                print(numList[i].description + " ",terminator: "")
            }
            print("\n")
        }
    }
    
    /*
     let personsMock: [PersonModel] = [
         PersonModel(id: 1, name: "佐藤浩介", age: 30, sex: "男性", location: "東京都", job: "会社員"),
         PersonModel(id: 2, name: "鈴木絵里", age: 21, sex: "女性", location: "大阪府", job: "大学生"),
         PersonModel(id: 3, name: "高橋美沙子", age: 33, sex: "女性", location: "静岡県", job: "主婦"),
         PersonModel(id: 4, name: "田中優作", age: 55, sex: "男性", location: "福岡県", job: "会社役員"),
         PersonModel(id: 5, name: "渡辺大毅", age: 17, sex: "男性", location: "埼玉県", job: "高校生"),
         PersonModel(id: 6, name: "中村真衣", age: 27, sex: "女性", location: "秋田県", job: "公務員"),
         PersonModel(id: 7, name: "山崎敏子", age: 72, sex: "女性", location: "広島県", job: "無職"),
         PersonModel(id: 8, name: "山田誠司", age: 46, sex: "男性", location: "北海道", job: "自営業")
     ]

     final class PersonListViewModel: ObservableObject {

         @Published private(set) var persons: [PersonModel] = []
         
         @Published var isShowDetail = false
         
         @Published private(set) var message = ""
         
         func loadPersons() {
             self.persons = personsMock
         }
         
         func showDetail(person: PersonModel) {
             var msg = "【氏名】：\(person.name)\n"
             msg += "【年齢】：\(person.age)歳\n"
             msg += "【性別】：\(person.sex)\n"
             msg += "【出身】：\(person.location)\n"
             msg += "【職業】：\(person.job)"
             self.message = msg
             
             self.isShowDetail = true
         }
     }
     */
}
