//
//  HomeTabView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/16.
//

import SwiftUI
import UIKit

struct HomeTabView: View {
    @State var isError: Bool = false
    @State var opaValue = 1.0
    @State var btnText = "消す"
    var body: some View {
        VStack {
            Image(systemName: "music.note.house")
                .scaleEffect(x: 3.0, y: 3.0)
                .frame(width: 100, height: 100)
            Text("HOME")
                .font(.system(size: 20))
                .opacity(opaValue)
            Button(action: {
                isError = true
            }) {
                Text("削除")
            }.alert(isPresented: $isError) {
                Alert(title: Text("○○を削除"),
                      message: Text("ほんとにいいの？"),
                      primaryButton: .destructive(Text("削除する"), action: {opaValue = 0.0}),
                      secondaryButton: .cancel(Text("キャンセル"), action: {}))
            }
            Button(action: {
                opaValue = 1.0
            }){
                Text("復活")
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color(red: 0.5, green: 0.9, blue: 0.9))
        .ignoresSafeArea()
    }
}
