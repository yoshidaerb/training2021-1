//
//  NewsTabView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/16.
//

import SwiftUI
import UIKit

struct NewsTabView: View {
    @State var isModal: Bool = false
    @State var counter:Int = 0
    var body: some View {
        VStack {
            Image(systemName: "newspaper")
                .scaleEffect(x: 3.0, y: 3.0)
                .frame(width: 100, height: 100)
            Text("ニュースと解説").font(.system(size: 20))
            Button(action: {
                isModal = true
            }) {
                Text("Sheetテスト")
            }
            .sheet(isPresented: $isModal, onDismiss: {counter += 1}) {
                SomeView()
            }
            // ハーフモーダルビューは３回まで
            .disabled(counter >= 3)
            Text("回数：\(counter)")
                .font(.title)
                .padding()
            // 回数リセットボタン
            Button(action: {
                counter = 0
            }){
                Text("回数リセット")
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color(red: 0.9, green: 0.9, blue: 0.8))
        .ignoresSafeArea()
    }
}



struct SomeView: View {
    var body: some View {
        VStack {
            Text("プレゼンテーション")
            Image(systemName: "gift")
                .imageScale(.large)
                .padding()
        }
    }
}
