//
//  WeatherTagView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/16.
//

import SwiftUI
import UIKit

struct WeatherTabView: View {
    @State var isSheet: Bool = false
    @State var backgroundcolor: Color = Color.clear
    var body: some View {
        ZStack {
            backgroundcolor
            VStack {
                Image(systemName: "cloud.sun")
                    .scaleEffect(x: 3.0, y: 3.0)
                    .frame(width: 100, height: 100)
                Text("お天気ページ").font(.system(size: 20))
                Button("背景色変更", action: {
                    self.isSheet = true
                })
                .actionSheet(isPresented: self.$isSheet) {() -> ActionSheet in
                    ActionSheet(title: Text("背景色"), message: Text("何色にしますか？"), buttons: [
                                    .default(Text("黄"), action: {self.backgroundcolor = Color.yellow}),
                                    .destructive(Text("赤"), action: {self.backgroundcolor = Color.red}),
                                    .default(Text("緑"), action: {self.backgroundcolor = Color.green}),
                                    .cancel(Text("戻す"), action: {self.backgroundcolor = Color.clear})])
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .ignoresSafeArea()
        }
    }
}
