//
//  ContentView9.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/15.
//

import SwiftUI
import UIKit

struct ContentView9: View {
    @State var selectedTag = 1
    var body: some View {
        TabView(selection: $selectedTag) {
            HomeTabView()
                .tabItem {
                    Image(systemName: "house")
                    Text("HOME")
                }.tag(1)
            WeatherTabView()
                .tabItem {
                    Image(systemName: "cloud.sun")
                    Text("天気")
                }.tag(2)
            NewsTabView()
                .tabItem {
                    Image(systemName: "newspaper")
                    Text("ニュース")
                }.tag(3)
        }
    }
}

/*
struct ContentView9: View {
    @State var selectedTag = 1
    var body: some View {
        TabView(selection: $selectedTag) {
            HomeTabView()
                .tabItem {
                    Image(systemName: "house")
                }.tag(1)
            WeatherTabView()
                .tabItem {
                    Image(systemName: "cloud.sun")
                }.tag(2)
            NewsTabView()
                .tabItem {
                    Image(systemName: "newspaper")
                }.tag(3)
        }
        .tabViewStyle(PageTabViewStyle())
        .ignoresSafeArea()
    }
}
*/
