//
//  kadai9View.swift
//  Training (iOS)
//
//  Created by 古橋省吾 on 2021/07/21.
//

import SwiftUI
import UIKit

struct kadai9View: View {
    @ObservedObject var viewModel = kadai9ViewModel()
    
    var body: some View {
        NavigationView{
            VStack(alignment: .center){
                HStack{
                    Text("商品コード:")
                    // 商品コード入力テキストフィールド
                    TextField("\(viewModel.code)",text:$viewModel.code)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width:150)
                }
                Group{
                    // 計算結果の表示
                    if viewModel.codeCheck(){
                        Text("")
                            .font(.title)
                    }else{
                        Text("商品コードが正しく入力できていません。")
                            .foregroundColor(.red)
                            .font(.headline)
                    }
                }
                .frame(width: 300, height: 30)
                HStack{
                    Text("商品名:")
                    // 商品名入力テキストフィールド
                    TextField("\(viewModel.name)",text:$viewModel.name)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width:150)
                }
                HStack{
                    Text("価格:")
                    // 価格入力テキストフィールド
                    TextField("\(viewModel.price)",text:$viewModel.price)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .keyboardType(.numberPad)
                        .frame(width:150)
                }
                Group{
                    // 計算結果の表示
                    if viewModel.priceCheck(){
                        Text("")
                            .font(.title)
                    }else{
                        Text("商品コードが正しく入力できていません。")
                            .foregroundColor(.red)
                            .font(.headline)
                    }
                }
                .frame(width: 300, height: 30)
                Button{
                    UIApplication.shared.endEdition()
                    viewModel.updateProduct()
                    viewModel.URL()
                }label:{
                    Text("実行")
                }
            }
        }
    }
}

