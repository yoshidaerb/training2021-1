//
//  kadai9Model.swift
//  Training (iOS)
//
//  Created by 古橋省吾 on 2021/07/21.
//

import Foundation
import RealmSwift

class kadai9Model: Object{
    @objc dynamic var 項目名: String? = nil
}
class Product: Object {
    @objc dynamic var code = ""
    @objc dynamic var name = ""
    @objc dynamic var price = 0
    
    override class func primaryKey() -> String! {
        return "code"
    }
}

class ProductControl {
    private var realm = try! Realm()
    
    func findByCode(_ product:Product) -> Product {
        let product2:Product = Product()
        do {
            let results = realm.objects(Product.self).filter("code == %@", product.code)
            // 検索結果の件数を取得します。
            let count = results.count
            if (count == 0) {
                // 検索データ0件の場合
            } else {
                // 検索データがある場合
                for result in results {
                    product2.code = result.code
                }
            }
        }catch{
            print("Error \(error)")
        }
        return product2
    }
    
    func add(_ product: Product) {
        try! realm.write {
            realm.add(product)
        }
    }
    
    func update(_ product: Product) {
        let product3 = realm.objects(Product.self).filter("code == %@", product.code).first
        do {
            try! realm.write {
                product3?.name = product.name
                product3?.price = product.price
            }
        }catch{
            print("Error \(error)")
        }
    }
}
