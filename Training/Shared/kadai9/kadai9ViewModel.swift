//
//  kadai9ViewModel.swift
//  Training (iOS)
//
//  Created by 古橋省吾 on 2021/07/21.
//

import Foundation
import UIKit
import RealmSwift
import Combine

//UIViewController , UITextFieldDelegate
class kadai9ViewModel: ObservableObject {
    @Published var code:String = ""
    @Published var name:String = ""
    @Published var price:String = ""
    
    init(product:Product = Product()){
        self.code = product.code
        self.name = product.name
        self.price = String(product.price)
    }
    
    // Realmをインスタンス化(データベースの作成)
    // Realmのインスタンスを取得
    //let realm = try! Realm()
    
    let productControl = ProductControl()
    
    func URL(){
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    // 商品コードチェック
    /// <#Description#>
    /// - Returns: <#description#>
    func codeCheck()->Bool{
        guard code.count == 8 else{
            return false
        }
        return true
    }
    // 価格チェック
    /// <#Description#>
    /// - Returns: <#description#>
    func priceCheck()->Bool{
        guard Int(price) != nil else{
            return false
        }
        return true
    }
    // 登録・更新実行
    func updateProduct(){
        let product = Product()
        guard !code.isEmpty else {
            return
        }
        product.code = code
        product.name = name
        product.price = Int(price) ?? 0
        let product1:Product = productControl.findByCode(product)
        if product1.code == "" {
            productControl.add(product)
        }else{
            productControl.update(product)
        }
    }
}

