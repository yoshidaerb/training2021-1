//
//  EnvironmentView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/20.
//

import SwiftUI

class ShareData: ObservableObject{
    @Published var isOn = false
    @Published var num = 1
}

struct EnvironmentView: View {
    // 共有オブジェクトを指定する
    @EnvironmentObject var setData: ShareData
    // シートが開いている状態
    @State var isShow: Bool = false
    var body: some View {
        VStack {
            // 現在の設定
            VStack(alignment: .leading, spacing: 5){
                Text("表示：\(setData.isOn ? "ON" : "OFF")")
                if setData.isOn {
                Text("評価：" + String(repeating: "★", count: setData.num))
                }
            }.font(.title2)
            // シートを表示するボタン
            Button("[ 設定を変更する ]"){
                isShow = true
            }
            .padding()
            .sheet(isPresented: $isShow) {
                // シートを作る
                SettingView(isPresented: $isShow)
            }
        }
    }
}

struct SettingView: View {
    // 共有オブジェクトを指定する
    @EnvironmentObject var setData: ShareData
    // シートが開いている状態
    @Binding var isPresented: Bool
    var body: some View {
        NavigationView {
            VStack {
                // スイッチ
                Toggle(isOn: $setData.isOn) {
                    Text("表示：\(setData.isOn ? "ON" : "OFF")")
                }.frame(width:250)
                // ステッパー (★の個数)
                Stepper(value: $setData.num, in: 1...5) {
                    Text("★：\(setData.num)")
                }
                .frame (width:250)
            }
            .font(.title2)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color(red: 0.9, green: 0.9, blue: 0.5))
            .ignoresSafeArea() // ビュー領域全体を塗る
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing){
                    // 閉じるボタン
                    Button("閉じる"){
                        isPresented = false
                    }
                    
                }
            }
        }
    }
}

