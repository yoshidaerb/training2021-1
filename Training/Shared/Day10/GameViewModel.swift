//
//  GameViewModel.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/20.
//

import Foundation
// Game クラス
class GameViewModel: ObservableObject {
    @Published var bet: String
    @Published var point: Int
    @Published var getPoint: Int
    init (bet:String = "", point:Int = 50, getPoint: Int = 0){
        self.bet = bet
        self.point = point
        self.getPoint = getPoint
    }
    // 賭けポイントのチェック
    func betCheck () {
        // 賭けポイントを Intに変換できないとき中断する
        guard var betPoint = Int(bet) else { return }
        // 賭けポイントの上限は point
        betPoint = min(betPoint, point)
        bet = String (betPoint)
    }
    
    // チャレンジ
    func challenge () {
        // 賭けポイントを Intに変換できないとき中断する
        guard let betPoint = Int(bet) else { return }
        let num = Int.random(in: -3...3)
        getPoint = betPoint * num
        point += getPoint
    }
    
    // 再チャレンジ (リスタート)
    func restart() {
        bet = ""
        point = 50
        getPoint = 0
    }
}
