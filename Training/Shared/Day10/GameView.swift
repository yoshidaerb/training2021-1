//
//  GameView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/20.
//

import SwiftUI

struct GameView: View {
    @ObservedObject var player = GameViewModel()
    var body: some View {
        VStack(alignment: .leading, spacing: 15){
            Text("現在のポイント：")
            Text("\(player.point)")
                .font(.title)
                .foregroundColor(player.point>0 ? .black : .red)
            TextField("賭けポイント", text: $player.bet)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .keyboardType(.numberPad)
                .onChange(of: player.bet, perform: { value in
                    player.betCheck()
                })
            Text("獲得ポイント")
            Text("\(player.getPoint)").font(.title)
            if player.point>0 {
                Button("チャレンジ！"){
                    player.challenge()
                }.padding()
            }else{
                Text("0以下です！").foregroundColor(.red)
                Button("[再チャレンジする]"){
                    player.restart()
                }.foregroundColor(.red).padding()
            }
        }
        .frame(width:250)
        .padding()
    }
}

