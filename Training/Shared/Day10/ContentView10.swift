//
//  Contentview10.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/19.
//

import SwiftUI

struct ContentView10: View {
    @State var selectedTag = 1
    var body: some View {
        TabView(selection: $selectedTag) {
            BindingView().tag(1)
            DismissView().tag(2)
            GameView().tag(3)
            EnvironmentView().environmentObject(ShareData()).tag(4)
        }
        .tabViewStyle(PageTabViewStyle())
        .ignoresSafeArea()
    }
}

