//
//  BindingView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/19.
//

import SwiftUI
import UIKit

struct BindingView: View {
    @State var isChecked_person1 = false
    @State var isChecked_person2 = false
    var body: some View {
        VStack{
            HStack{
                Text("担当者１のチェック")
                PersonCheckMark(isChecked: $isChecked_person1)
            }
            HStack{
                Text("担当者２のチェック")
                PersonCheckMark(isChecked: $isChecked_person2)
            }
            // 全チェック判定
            if isChecked_person1 && isChecked_person2{
                Text("全員チェック済み")
                    .foregroundColor(.blue)
                    .padding()
            }else{
                Text("チェック待ち")
                    .foregroundColor(.red)
                    .padding()
            }
        }
    }
}

struct PersonCheckMark: View {
    // ContentViewビューの変数とバインディングする変数
    @Binding var isChecked: Bool
    var body: some View {
        Button(action:{
            // isCheckedのtrue/falseを反転する
            isChecked.toggle()
        }){
            // isCheckedがtrueかfalseかでイメージと色を選ぶ
            Image(systemName: isChecked ?
                "person.crop.circle.badge.checkmark" :
                "person.crop.circle")
                .foregroundColor(isChecked ? .blue : .gray)
        }
        .scaleEffect(CGSize(width: 2.0, height: 2.0))
        .frame(width: 50, height: 50, alignment: .center)
    }
}
