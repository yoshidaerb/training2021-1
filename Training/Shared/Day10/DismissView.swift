//
//  DismissView.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/19.
//

import SwiftUI

struct DismissView: View {
    @State var isShow: Bool = false
    var body: some View {
        Button(action: {
            isShow = true
        }) {
            Text("シートを表示")
        }
        .fullScreenCover(isPresented: $isShow){
            SomeView1(isPresented: $isShow)
        }
    }
}

struct SomeView1: View {
    @Binding var isPresented: Bool
    var body: some View {
        NavigationView {
            VStack {
                Image(systemName: "ladybug").scaleEffect(2.0)
                Text("ハロー").font(.title2).padding()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color(red:0.9,green:0.9,blue:0.8))
            .ignoresSafeArea()
            .toolbar{
                ToolbarItem(placement: .navigationBarTrailing){
                    Button {
                        isPresented = false
                    } label: {
                        Text("閉じる")
                    }
                }
            }
        }
    }
}


