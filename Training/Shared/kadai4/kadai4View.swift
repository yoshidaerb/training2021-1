//
//  kadai4View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import SwiftUI

struct kadai4View: View {
    // アラート表示状態を定義
    @State var isPresented: Bool = false
    
    var body: some View {
        // 実行ボタン
        Button(action: {
            //押下でアラートを表示
            isPresented = true
        }) {
            Text("実行")
        }.alert(isPresented: $isPresented) {
            // アラートのタイトル：「糖質オフしますか？」
            Alert(title: Text("糖質オフしますか？"),
                  // OKもCancelも押下するとアラートを閉じる
                  primaryButton: .destructive(Text("OK"), action: {}),
                  secondaryButton: .cancel(Text("Cancel"), action: {}))
        }
    }
}


