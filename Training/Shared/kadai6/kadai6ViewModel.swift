//
//  kadai6ViewModel.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import Foundation

class kadai6ViewModel: ObservableObject {
    // 変更をパブリッシュするプロパティ
    @Published var code:String = ""
    @Published var name:String = ""
}
