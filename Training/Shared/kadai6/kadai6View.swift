//
//  kadai6View.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/27.
//

import SwiftUI

struct kadai6View: View {
    // 観測するオブジェクト
    @ObservedObject var viewModel = kadai6ViewModel()
    // テキストフィールドの値を格納する場所
    @State var code = ""
    @State var name = ""
    
    var body: some View {
        VStack{
            // 商品コード随時更新表示
            Text(viewModel.code).frame(width:200)
            // 商品名随時更新表示
            Text(viewModel.name).frame(width:200)
            // 子View
            ChildView(code:$code,name:$name)
            // 実行ボタン
            Button(action: {
                // テキストフィールドの値をObservedObjectに代入
                viewModel.code = code
                viewModel.name = name
            }) {
                Text("実行")
            }
            .frame(width:200)
        }
    }
}

struct ChildView: View {
    // kadai6Viewの変数とバインディングする変数
    @Binding var code:String
    @Binding var name:String
    
    var body: some View {
        VStack{
            // 商品コード入力テキストフィールド
            TextField("商品コード",text:$code)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .frame(width:200)
            // 商品名入力テキストフィールド
            TextField("商品名",text:$name)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .frame(width:200)
        }
    }
}
