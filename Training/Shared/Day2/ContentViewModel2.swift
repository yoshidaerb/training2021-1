//
//  ContentViewModel2.swift
//  Training
//
//  Created by 野呂彩香 on 2021/07/06.
//

import Foundation

class ContentViewModel2: ObservableObject {
    // 半径
    @Published var radius = 10.0
    // 直径
    var diameter:Double {
        // 半径から直径を計算して返す
        get{
            radius * 2
        }
        // 直径から半径を計算してセットする
        set (length){
            radius = length / 2
        }
    }
    // 直径を4、半径を2増やす
    func updateDiameter(){
        diameter += 4
    }
}
