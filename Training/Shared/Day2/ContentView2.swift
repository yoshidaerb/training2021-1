//
//  ContentView2.swift
//  Training
//
//  Created by 野呂彩香 on 2021/07/06.
//

import SwiftUI
import UIKit

struct ContentView2: View {
    @ObservedObject var viewModel2 = ContentViewModel2()
    var body: some View {
        VStack(spacing:15.0){
            Text("ccs")
                .font(.system(size: 100)).fontWeight(.black)
                .foregroundColor(.red)
                .frame(width: 200.0, height: 100)
                .border(Color.black, width: 5)
                .background(Color.green)
            Spacer()
            Text("今は昔、竹取の翁といふものありけり。野山にまじりて竹を取りつつ、よとづのことに使ひけり。名をば、さぬきのみやつことなむいひける。")
                .frame(width: 200.0)
                .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
            Text("半径：\(viewModel2.radius)")
            Text("直径：\(viewModel2.diameter)")
            Button(action: {
                // viewModelのアクションを呼び出す
                viewModel2.updateDiameter()
            }){
                Text("半径+2")
                    .font(.largeTitle)
            }
            Spacer()
            Text("NTT-DATA CCS")
                .frame(width: 300, alignment: .bottomTrailing)
                .padding()
        }
    }
}
