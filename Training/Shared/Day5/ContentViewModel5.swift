//
//  ContentViewModel5.swift
//  Training (iOS)
//
//  Created by 古橋省吾 on 2021/07/12.
//

import Foundation

class ContentViewModel5: ObservableObject {
    @Published var message1:String = "僕の誕生日は？"
    @Published var message:String = ""
    @Published var month:Int = 1
    @Published var date:Int = 1
    
    // 月日のリセット
    func reset(){
        month = 1
        date = 1
    }
    
    // 月日の値のチェック
    func checkNumUpdateMessage(){
        if(month == 10 && date == 13){
            message = "大正解です！"
        }else{
            message = "違いますー"
        }
    }
}
