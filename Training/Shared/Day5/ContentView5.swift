//
//  ContentView5.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/12.
//

import SwiftUI
import UIKit

struct ContentView5: View {
    @ObservedObject var viewModel5 = ContentViewModel5()
    @State var R:Double = 0
    @State var G:Double = 0
    @State var B:Double = 0
    @State var A:Double = 1
    var body: some View {
        VStack(alignment: .center){
            Text(viewModel5.message1)
            Text(viewModel5.message)
            
            Stepper(value: $viewModel5.month, in: 1...12){
                Text("Month：\(viewModel5.month)")
            }
            .frame(width:200)
            
            Stepper(value: $viewModel5.date, in: 1...31){
                Text("Date：\(viewModel5.date)")
            }
            .frame(width:200)
            
            Button(action: {
                /// 月日のリセット
                viewModel5.reset()
            }){
                Text("リセット")
                    .font(.largeTitle)
            }
            Button(action: {
                // 月日の値のチェック
                viewModel5.checkNumUpdateMessage()
            }){
                Text("決定")
                    .font(.largeTitle)
            }
        }
        ZStack{
            Image(systemName: "ladybug")
                .scaleEffect(3)
            Circle()
                .frame(width: 100, height: 100)
                .padding()
                .foregroundColor(Color(red: R/255, green: G/255, blue: B/255, opacity: A))
        }
        // redのスライダー
        HStack{
            Circle()
                .foregroundColor(.red)
                .frame(width: 20, height: 20)
            Text(String(Int(R))).frame(width: 40)
            Slider(value: $R, in: 0...255).frame(width: 200)
        }
        // greenのスライダー
        HStack{
            Circle()
                .foregroundColor(.green)
                .frame(width: 20, height: 20)
            Text(String(Int(G))).frame(width: 40)
            Slider(value: $G, in: 0...255).frame(width: 200)
        }
        // blueのスライダー
        HStack{
            Circle()
                .foregroundColor(.blue)
                .frame(width: 20, height: 20)
            Text(String(Int(B))).frame(width: 40)
            Slider(value: $B, in: 0...255).frame(width: 200)
        }
        // opacityのスライダー
        HStack{
            Rectangle()
                .stroke(lineWidth: 2)
                .foregroundColor(.blue)
                .frame(width: 18, height: 18)
            Text(String(round(A*10)/10)).frame(width:40)
            Slider(value: $A).frame(width: 200)
        }
    }
}

