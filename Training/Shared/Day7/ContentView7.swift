//
//  ContentView7.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/14.
//

import SwiftUI
import UIKit

struct ContentView7: View {
    @State var selectedSize = 2
    @State var selectedColor = 0
    @State var sizes = ["XS","S","M","L","LL"]
    @State var colors = ["Red","Green","Blue","Yellow","Pink","White"]
    @State var theDate = Date()
    var body: some View {
        NavigationView{
            VStack(alignment: .center){
                Form{
                    Section(header:Text("サイズ").font(.headline),footer:Text("USサイズの少し大きめです")){
                        // Sizeのピッカー
                        Picker(selection: $selectedSize, label: Text("Size")){
                            ForEach(0..<sizes.count){index in
                                Text(sizes[index])
                            }
                        }
                        Text("選んだサイズ：\(sizes[selectedSize])")
                    }
                    Section(header: Text("色").font(.headline)){
                        // Colorのピッカー
                        Picker(selection: $selectedColor, label: Text("Color")){
                            ForEach(0..<colors.count){index in
                                Text(colors[index])
                            }
                        }
                        Text("選んだ色：\(colors[selectedColor])")
                    }
                }
                .frame(height:300)
                .navigationTitle(Text("サイズと色"))
                .navigationBarTitleDisplayMode(.inline)
                VStack(alignment: .center){
                    // 日付のピッカー
                    DatePicker("日付",selection:$theDate,displayedComponents:.date)
                        .environment(\.locale, Locale(identifier: "ja_JP"))
                        .frame(width:200)
                    // 時刻のピッカー
                    DatePicker("時刻",selection:$theDate,displayedComponents:.hourAndMinute)
                        .environment(\.locale, Locale(identifier: "ja_JP"))
                        .frame(width:200)
                    Text(theDate.description(with: Locale(identifier: "ja_JP")))
                        .font(.footnote)
                }
                .padding()
                .border(Color.gray,width:1)
            }
        }
    }
}
