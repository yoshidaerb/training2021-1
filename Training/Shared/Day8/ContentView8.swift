//
//  ContentView8.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/14.
//

import SwiftUI
import UIKit

extension UIApplication{
    // キーボードを下げる
    func endEdition() {
        sendAction(
            #selector(UIResponder.resignFirstResponder),
            to: nil, from: nil, for: nil
        )
    }
}

struct ContentView8: View {
    @ObservedObject var viewModel8 = ContentViewModel8()
    var body: some View {
        ZStack{
            // 背景のタップでキーボードを下げる
            Color.white // 背景色
                .onTapGesture {
                    UIApplication.shared.endEdition()
            }
            VStack(alignment: .leading){
                // 入力テキストフィールド
                HStack{
                    Text("個数：").padding(.horizontal,0)
                    TextField("0",text:$viewModel8.kosu)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .keyboardType(.numberPad)
                        .frame(width:80)
                }
                .font(.title)
                .frame(width:300)
                
                // 計算結果の表示
                Group{
                    if viewModel8.kosuCheck(min:1,max:10){
                        Text("\(viewModel8.priceCalc())円です。")
                            .font(.title)
                    }else{
                        Text("個数は1~10個を入れてください。")
                            .foregroundColor(.red)
                            .font(.headline)
                    }
                }
                .frame(width: 300, height: 30)
            }
        }
        NavigationView{
            TextEditor(text:$viewModel8.theText)
                .lineSpacing(10)
                .border(Color.gray)
                .padding([.leading, .bottom, .trailing])
                .navigationTitle("メモ")
                .toolbar{
                    // 読み込みボタン
                    ToolbarItem(placement: .navigationBarTrailing){
                        Button{
                            if let data = viewModel8.loadText("training.txt"){
                                viewModel8.theText = data
                            }
                        }label:{
                            Text("読み込み")
                        }
                    }
                    // 保存ボタン
                    ToolbarItem(placement: .navigationBarTrailing){
                        Button{
                            UIApplication.shared.endEdition()
                            viewModel8.saveText(viewModel8.theText, "training.txt")
                        }label:{
                            Text("保存")
                        }
                    }
                }
        }
    }
}

