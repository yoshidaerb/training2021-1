//
//  ContentViewModel8.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/15.
//

import Foundation

class ContentViewModel8: ObservableObject {
    @Published var kosu:String = ""
    let tanka:Double = 250
    let tax:Double = 1.1
    @Published var theText:String = ""
    // 個数チェック
    func kosuCheck(min:Int,max:Int)->Bool{
        guard let num = Int(kosu) else{
            return false
        }
        return (min...max).contains(num)
    }
    
    // 料金計算
    func priceCalc() -> Int{
        if let num = Double(kosu){
            let result = Int(tanka * num * tax)
            return result
        }else{
            return -1
        }
    }
    
    // 保存ファイルへのURLを作る
    func docURL(_ fileName:String) -> URL?{
        let fileManager = FileManager.default
        do {
            // Documentsフォルダ
            let docsURL = try fileManager.url(
                for: .documentDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: false)
            // URLを作る
            let url = docsURL.appendingPathComponent(fileName)
            return url
        }catch{
            return nil
        }
    }

    // テキストデータを保存する
    func saveText(_ textData:String, _ fileName:String){
        guard let url = docURL(fileName) else {
            return
        }
        do{
            let path = url.path
            try textData.write(toFile: path, atomically: true, encoding: .utf8)
        }catch let error as NSError{
            print(error)
        }
    }

    // テキストデータを読み込んで返す
    func loadText(_ fileName:String) -> String? {
        guard let url = docURL(fileName) else {
            return nil
        }
        do{
            let textData = try String(contentsOf: url, encoding: .utf8)
            return textData
        }catch{
            return nil
        }
    }
}
