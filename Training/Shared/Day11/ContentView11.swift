//
//  ContentView11.swift
//  Training
//
//  Created by 古橋省吾 on 2021/07/20.
//

import SwiftUI
import MapKit

struct Spot: Identifiable {
    let id = UUID()
    let name: String
    let latitude: Double
    let longitude: Double
    var coordinate: CLLocationCoordinate2D{
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

struct ContentView11: View {
    // スポット
    let spotList = [
        Spot(name: "NTTDATACCS", latitude: 35.6084321, longitude: 139.7478793)
    ]
    // managerの更新を観測する
    @ObservedObject var manager = LocationManager()
    // ユーザートラッキングモード (追従モード)
    @State var trackingMode = MapUserTrackingMode.follow
    var body: some View {
        // 現在地を追従する地図を表示する
        Map(coordinateRegion: $manager.region,
            showsUserLocation: true,
            userTrackingMode: $trackingMode,
            annotationItems: spotList,
            annotationContent: {spot in
                MapAnnotation(coordinate: spot.coordinate,
                              anchorPoint: CGPoint(x:0.5,y:0.5),
                              content: {
                                Image(systemName: "house.fill").foregroundColor(.red)
                                Text(spot.name).italic()
                              })}
        ).edgesIgnoringSafeArea(.bottom)
    }
}

// 現在地を取得するためのクラス
class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    // ロケーションマネージャを作る
    let manager = CLLocationManager()
    // 領域の更新をパブリッシュする
    @Published var region = MKCoordinateRegion()
    override init() {
        super.init()
        manager.delegate = self // デリゲートの設定
        manager.requestWhenInUseAuthorization() // プライバシー設定の確認
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = 2 // 更新距離(m)
        manager.startUpdatingLocation()
    }
    
    // 領域の更新 (デリゲートメソッド)
    func locationManager (_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        // loacationsの最後の要素に対して実行する
        locations.last.map {
            let center = CLLocationCoordinate2D(
                latitude: $0.coordinate.latitude,
                longitude: $0.coordinate.longitude)
            // 領域の更新
            region = MKCoordinateRegion(
                center: center,
                latitudinalMeters: 1000.0,
                longitudinalMeters: 1000.0
            )
        }
    }
}
